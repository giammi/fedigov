# Fedigov - Public communication for authorities

Webpage to promote public communication and federated services for public authorities and organizations.

![Fedigov-Logo](static/images/logos/fedigov_logo.svg)
